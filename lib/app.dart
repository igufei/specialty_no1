import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:specialty_no1/pages/main_page.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/effect.dart';

/// app主框架入口
class MyApp extends StatelessWidget {
  MyApp() {
    Routes.setup(FluroRouter());
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '特产1号',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: MyColors.mainColor,
      ),
      home: MainPage(),
      supportedLocales: [
        Locale('zh', 'CH'),
        Locale('en', 'US'),
      ],
      localizationsDelegates: [
        //此处
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        FallbackCupertinoLocalisationsDelegate(),
      ],
      locale: Locale('zh'),
    );
  }
}

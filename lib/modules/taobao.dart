import 'package:dio/dio.dart';

Dio dio = new Dio();

class Taobao {
  static Future<dynamic> getDetailInfo(String itemID) async {
    String url =
        "https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.4.8&appKey=12574478&t=1575518434204&api=mtop.taobao.detail.getdetail&v=6.0&dataType=jsonp&ttid=2017%40taobao_h5_6.6.0&AntiCreep=true&type=jsonp&data=%7B%22itemNumId%22%3A%22$itemID%22%7D";
    var res = await dio.get(url);
    return res.data;
  }

  static Future<List> getDetailImage(String itemID) async {
    List images = [];
    String url =
        "http://h5api.m.taobao.com/h5/mtop.taobao.detail.getdesc/6.0/?jsv=2.4.11&appKey=12574478&t=${DateTime.now().millisecondsSinceEpoch}&api=mtop.taobao.detail.getdesc&v=6.0&type=jsonp&dataType=jsonp&timeout=20000&data=%7B%22id%22%3A%22$itemID%22%2C%22type%22%3A%220%22%2C%22f%22%3A%22%22%7D";
    var res = await dio.get(url);
    try {
      List pages = res.data["data"]["wdescContent"]["pages"] as List;
      for (String item in pages) {
        Match match = new RegExp(r">([\s\S]+?)<").firstMatch(item);
        if (match != null) {
          images.add(match.group(1));
        }
      }
    } catch (e) {}

    return images;
  }
}

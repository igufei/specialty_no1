import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 自适配尺寸计算
class MySize {
  /// 按照设计稿宽来设计值
  static double V(double width) => ScreenUtil().setWidth(width);

  /// 按照设计稿宽来设计值
  static double W(double width) => ScreenUtil().setWidth(width);

  /// 按照设计稿高来设计值
  static double H(double height) => ScreenUtil().setHeight(height);

  /// 设计字体大小
  static double S(double fontSize) => ScreenUtil().setSp(fontSize);

  static double get appBarTitleFontSize => ScreenUtil().setSp(28);
  static double get appBarHeight => ScreenUtil().setHeight(60);

  static double get pixelRatio => ScreenUtil.pixelRatio; //设备的像素密度
  static double get screenWidth => ScreenUtil.screenWidth; //设备宽度
  static double get screenHeight => ScreenUtil.screenHeight; //设备高度
  static double get bottomBarHeight => ScreenUtil.bottomBarHeight; //底部安全区距离，适用于全面屏下面有按键的
  static double get statusBarHeight => ScreenUtil.statusBarHeight; //状态栏高度 刘海屏会更高  单位px
  static double get textScaleFactory => ScreenUtil.textScaleFactory; //系统字体缩放比例

  static double get scaleWidth => ScreenUtil().scaleWidth; // 实际宽度的dp与设计稿px的比例
  static double get scaleHeight => ScreenUtil().scaleHeight; // 实际高度的dp与设计稿px的比例
}

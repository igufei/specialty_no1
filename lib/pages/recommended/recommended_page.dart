import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/recommended/widgets/great_goods.dart';
import 'package:specialty_no1/pages/recommended/widgets/ranking_list.dart';
import 'package:specialty_no1/pages/recommended/widgets/recommended.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/effect.dart';
/// 推荐页面
class RecommendedPage extends StatefulWidget {
  final String tabIndex;

  const RecommendedPage({Key key, this.tabIndex = "0"}) : super(key: key);
  @override
  _RecommendedPageState createState() => _RecommendedPageState();
}

class _RecommendedPageState extends State<RecommendedPage> with SingleTickerProviderStateMixin {
  final List _tabItems = ["为您推荐", "排行榜", "有好货"];
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: _tabItems.length,
    );

    _tabController.index = int.tryParse(widget.tabIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bgColor,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(MySize.V(90)),
        child: AppBar(
          title: Text(
            "推荐",
            style: TextStyle(fontSize: MySize.appBarTitleFontSize),
          ),
          centerTitle: true,
          bottom: TabBar(
            controller: _tabController,
            labelPadding: EdgeInsets.symmetric(vertical: MySize.V(5)),
            tabs: _tabItems
                .map((item) => Text(
                      item,
                      style: TextStyle(fontSize: MySize.S(22)),
                    ))
                .toList(),
            onTap: null,
          ),
        ),
      ),
      body: ScrollConfiguration(
        behavior: OverScrollBehavior(),
        child: TabBarView(
          controller: _tabController,
          children: <Widget>[
            RecommendedWidget(),
            RankingListWidget(),
            GreatGoodsWidget(),
          ],
        ),
      ),
    );
  }
}

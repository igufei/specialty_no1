import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/models/keyword.dart';
import 'package:specialty_no1/widgets/goods/goods_item.dart';
import 'package:specialty_no1/widgets/goods/goods_view.dart';
import 'package:specialty_no1/widgets/goods/goods_model.dart';
/// 有好货
class GreatGoodsWidget extends StatefulWidget {
  @override
  _GreatGoodsWidgetState createState() => _GreatGoodsWidgetState();
}

class _GreatGoodsWidgetState extends State<GreatGoodsWidget> with AutomaticKeepAliveClientMixin {
  final GoodsModel _model = GoodsModel();
  @override
  void initState() {
    super.initState();
    _model.setValue(keyword: Keyword.greatGoods);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: _model)],
      child: Container(
        child: Consumer<GoodsModel>(
          builder: (context, model, widget) => GoodsView(
            model: _model,
            layoutType: LayoutType.Column
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

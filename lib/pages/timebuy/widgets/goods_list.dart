import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_model.dart';
import 'package:specialty_no1/widgets/countdown.dart';
import 'package:specialty_no1/widgets/goods/goods_model.dart';
import 'package:specialty_no1/widgets/goods/goods_view.dart';

/// 限时抢购页面的商品展示部分
class GoodsListWidget extends StatefulWidget {
  final int pageNum;
  final String keyword;
  final TimeBuyTabItemModel tabItemModel;
  GoodsListWidget(this.pageNum, this.keyword, this.tabItemModel);
  @override
  _GoodsListWidgetState createState() => _GoodsListWidgetState();
}

class _GoodsListWidgetState extends State<GoodsListWidget> with AutomaticKeepAliveClientMixin {
  final GoodsModel _goodsModel = GoodsModel();
  @override
  void initState() {
    super.initState();
    _goodsModel.setValue(pageNum: widget.pageNum, keyword: widget.keyword);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    TimeBuyTabItemModel tabItemModel = widget.tabItemModel;

    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: _goodsModel)],
      child: Column(
        children: <Widget>[
          GoodsListHeaderWidget(
            tabItemModel: tabItemModel,
          ),
          Expanded(
            child: Consumer<GoodsModel>(
              builder: (context, model, widget) => GoodsView(
                model: _goodsModel,
                viewStyle: GoodsViewStyle.timeBuy,
                enablePullDown: false,
                enablePullUp: false,
                timeState: tabItemModel.timeState,
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

/// 限时抢购页面倒计时展示部分
class GoodsListHeaderWidget extends StatefulWidget {
  final TimeBuyTabItemModel tabItemModel;

  const GoodsListHeaderWidget({Key key, @required this.tabItemModel}) : super(key: key);
  @override
  _GoodsListHeaderWidgetState createState() => _GoodsListHeaderWidgetState();
}

class _GoodsListHeaderWidgetState extends State<GoodsListHeaderWidget> {
  String _headTitle = "";
  Widget _headWidget = Container();
  @override
  void initState() {
    super.initState();
    if (widget.tabItemModel.timeState == TimeState.past) {
      _headTitle = "还有剩余好货，赶紧抢哦";
    } else if (widget.tabItemModel.timeState == TimeState.now) {
      _headTitle = "抢购进行时";
      _headWidget = _buldHeadWidget("距结束", widget.tabItemModel.endTime);
    } else {
      _headTitle = "开抢倒计时";
      _headWidget = _buldHeadWidget("距开始", widget.tabItemModel.startTime);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MySize.V(50),
      padding: EdgeInsets.only(left: 5, right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Text(
              _headTitle,
              style: TextStyle(fontSize: MySize.S(25)),
            ),
          ),
          _headWidget,
        ],
      ),
    );
  }

  Widget _buldHeadWidget(String text, DateTime time) {
    return Container(
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 5),
            child: Text(
              text,
              style: TextStyle(fontSize: MySize.S(22)),
            ),
          ),
          CountdownWidget(
            endTime: time,
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_model.dart';
import 'package:specialty_no1/widgets/effect.dart';
/// 限时抢购页面的顶部栏
class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  //final int index;
  final List<TimeBuyTabItemModel> items;
  final void Function(int index) onTap;
  final TabController controller;
  const MyAppBar({Key key, @required this.items, this.onTap, this.controller}) : super(key: key);
  @override
  _MyAppBarState createState() => _MyAppBarState();

  @override
  Size get preferredSize => Size(double.infinity, MySize.V(130));
}

class _MyAppBarState extends State<MyAppBar> {
  int _index=0;
  ScrollController _sController = ScrollController();
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      setState(() {
        _index = widget.controller.index;
      });
       _sController.animateTo(MySize.V(108) * _index - MySize.V(108) * 2, duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: MySize.statusBarHeight),
      decoration: BoxDecoration(
        boxShadow: [BoxShadow(color: Color(0xffbfbfbf), offset: Offset(0, 2), blurRadius: 5.0)],
        image: DecorationImage(
          alignment: Alignment.topCenter,
          image: AssetImage("assets/images/timebuybg.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: MySize.V(50),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: BackButton(color: Colors.white),
                  ),
                ),
                Expanded(flex: 2, child: Image.asset("assets/images/timebuy.png")),
                Expanded(child: Container())
              ],
            ),
          ),
          Expanded(
            child: ScrollConfiguration(
              behavior: OverScrollBehavior(),
              child: ListView(
                controller: _sController,
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: widget.items.asMap().keys.map((index) {
                  Color textColor = Colors.white70;
                  FontWeight fontWeight = FontWeight.w300;
                  if (_index == index) {
                    textColor = Colors.white;
                    fontWeight = FontWeight.w500;
                  }
                  return GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(4),
                      width: MySize.V(108),
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Text(
                              widget.items[index].title,
                              style: TextStyle(fontSize: MySize.S(26), color: textColor, fontWeight: fontWeight),
                            ),
                          ),
                          Container(
                            child: Text(
                              widget.items[index].stateText,
                              style: TextStyle(fontSize: MySize.S(17), color: textColor, fontWeight: fontWeight),
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () => _tapItem(context, index),
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _tapItem(BuildContext context, int index) {
    setState(() {
      _index = index;
      widget.controller.index = index;
    });
    _sController.animateTo(MySize.V(108) * _index - MySize.V(108) * 2, duration: Duration(milliseconds: 500), curve: Curves.ease);
    widget.onTap(index);
  }
}

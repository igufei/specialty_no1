/// 时间状态 表示 过去 、现在 、未来
enum TimeState { past, now, futher }

/// 限时抢购tab项数据模型
class TimeBuyTabItemModel {
  String title;
  DateTime startTime;
  DateTime endTime;
  TimeState timeState;
  String stateText;

  TimeBuyTabItemModel(
    this.title,
    this.stateText,
    this.startTime,
    this.endTime,
    this.timeState,
  );
}

/// 限时抢购页面数据 模型
class TimeBuyModel {
  List<TimeBuyTabItemModel> _items = [
    TimeBuyTabItemModel(
      "8:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 8),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "10:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 10),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 12),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "12:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 12),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 14),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "14:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 14),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 16),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "16:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 16),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 18),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "18:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 18),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 20),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "20:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 20),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 22),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "22:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 22),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 0),
      TimeState.past,
    ),
    TimeBuyTabItemModel(
      "00:00",
      "",
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 0),
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 1, 8),
      TimeState.past,
    ),
  ];
  int currentIndex = 0;
  DateTime currentActivityEndTime;
  TimeBuyModel() {
    DateTime now = DateTime.now();
    for (int index = 0; index < _items.length; index++) {
      DateTime time = _items[index].startTime;
      DateTime nextTime = _items[index].endTime;
      /* if (index == _items.length - 1) {
        // 如果是最后一个元素就把下一个下标设置为0
        nextTime = _items[0].startTime;
        nextTime = nextTime.add(Duration(days: 1));
      } else {
        int nextIndex = index + 1;
        nextTime = _items[nextIndex].startTime;
      } */

      if (time.isBefore(now) && nextTime.isBefore(now)) {
        // 状态1【抢购结束】当时结点时间与下一个结点时间都小于现在时间
        _items[index].stateText = "已经开抢";
        _items[index].timeState = TimeState.past;
      } else if (time.isBefore(now) && nextTime.isAfter(now)) {
        // 状态1【抢购结束】当时结点时间小于等于现在时间且下一个结点时间都大于现在时间
        _items[index].stateText = "正在抢购";
        _items[index].timeState = TimeState.now;
        currentIndex = index;
        currentActivityEndTime = _items[index].endTime;
      } else {
        _items[index].stateText = "即将抢购";
        _items[index].timeState = TimeState.futher;
      }
    }
  }
  List<TimeBuyTabItemModel> get items => _items;

}

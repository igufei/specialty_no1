import 'package:flutter/material.dart';
import 'package:specialty_no1/models/keyword.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_model.dart';
import 'package:specialty_no1/pages/timebuy/widgets/goods_list.dart';
import 'package:specialty_no1/pages/timebuy/widgets/my_app_bar.dart';
import 'package:specialty_no1/widgets/effect.dart';
/// 限时抢购页面
class TimeBuyPage extends StatefulWidget {
  @override
  _TimeBuyPageState createState() => _TimeBuyPageState();
}

class _TimeBuyPageState extends State<TimeBuyPage> with SingleTickerProviderStateMixin {
  TimeBuyModel _model = TimeBuyModel();
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: _model.items.length,
    );
    _tabController.animateTo(_model.currentIndex, duration: Duration(milliseconds: 0));
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        items: _model.items,
        controller: _tabController,
        onTap: (index) {},
      ),
      body: Container(
        child: ScrollConfiguration(
          behavior: OverScrollBehavior(),
          child: TabBarView(
            controller: _tabController,
            children: _model.items.asMap().keys.map((index) => GoodsListWidget(index + 1, Keyword.timeBuy, _model.items[index])).toList(),
          ),
        ),
      ),
    );
  }
}

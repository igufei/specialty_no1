import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';

/// chip 小部件 有来展示历史关键词与推荐关键词
class Chips extends StatefulWidget {
  final String title;
  final List<String> chips;
  final void Function(String keyword) onTap;
  final void Function() onTapDelete;
  Chips({
    Key key,
    @required this.title,
    @required this.chips,
    @required this.onTap,
    this.onTapDelete,
  }) : super(key: key);
  @override
  _ChipsState createState() => _ChipsState();
}

class _ChipsState extends State<Chips> {
  @override
  Widget build(BuildContext context) {
    if (widget.chips == null || widget.chips.length == 0) {
      return Container();
    }
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(color: MyColors.gray, fontSize: MySize.S(22)),
              ),
              Builder(
                builder: (context) {
                  if (widget.onTapDelete == null) {
                    return Container();
                  }
                  return GestureDetector(
                    child: Icon(Icons.delete_sweep, color: MyColors.gray, size: MySize.S(34)),
                    onTap: () => widget.onTapDelete(),
                  );
                },
              ),
            ],
          ),
          Container(
            width: double.infinity,
            child: Wrap(
              alignment: WrapAlignment.start,
              spacing: MySize.V(15),
              children: widget.chips
                  .map(
                    (item) => Chip(
                      backgroundColor: Color(0xfff0f0f0),
                      label: GestureDetector(
                        child: Text(
                          item,
                          style: TextStyle(fontSize: MySize.S(18), color: Color(0xff333333)),
                        ),
                        onTap: () => widget.onTap(item),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}

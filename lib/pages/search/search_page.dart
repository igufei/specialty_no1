import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/modules/utils.dart';
import 'package:specialty_no1/pages/search/search_model.dart';
import 'package:specialty_no1/pages/search/widgets/chips.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/effect.dart';
import 'package:specialty_no1/widgets/search_bar_button.dart';
/// 商品搜索页面
class SearchPage extends StatelessWidget {
  final TextEditingController _controller = TextEditingController();
  final _model = SearchModel();
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: _model)],
      child: Scaffold( 
        appBar: PreferredSize(
          child: AppBar(
            title: SearchBarButton(
              controller: _controller,
              onSubmitted: (text) => _tapSearchText(context, text),
            ),
            centerTitle: true,
            titleSpacing: 0,
            actions: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(right: MySize.V(20)),
                child: GestureDetector(
                  child: Text(
                    "搜索",
                    style: TextStyle(fontSize: MySize.S(24)),
                  ),
                  onTap: () => _tapSearchText(context, _controller.text),
                ),
              )
            ],
          ),
          preferredSize: Size.fromHeight(MySize.appBarHeight),
        ),
        body: Container(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: ScrollConfiguration(
            behavior: OverScrollBehavior(),
            child: Consumer(
              builder: (context, SearchModel model, _) => ListView(
                children: <Widget>[
                  Chips(
                    title: "最近搜索",
                    chips: _model.historyChips,
                    onTap: (item) => _tapKeywordChip(context, item),
                    onTapDelete: () => _model.clearHistoryChips(),
                  ),
                  Chips(
                    title: "猜你想搜",
                    chips: _model.guessChips,
                    onTap: (item) => _tapKeywordChip(context, item),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// 点击搜索文本
  void _tapSearchText(BuildContext context, String keyword) {
    if (keyword != "") {
      _model.addHistoryChips(keyword);
      _navigateToGoodsListPage(context, keyword);
    }
  }

  /// 点击关键词Chip
  void _tapKeywordChip(BuildContext context, String keyword) {
    if (keyword != "") {
      _navigateToGoodsListPage(context, keyword);
    }
  }

  /// 导航到商品列表页面
  void _navigateToGoodsListPage(BuildContext context, String keyword) {
    keyword = MyText.encode(keyword);
    Application.router.navigateTo(context, "/list?title=$keyword&keyword=$keyword", transition: TransitionType.cupertino);
  }
}

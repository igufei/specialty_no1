import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
/// 商品搜索页面数据处理
class SearchModel extends ChangeNotifier {
  List<String> _historyChips = [];
  List<String> _guessChips = ["地方特产", "闽南肉粽", "同安封肉", "正山小种", "台湾特产", "高粱酒", "商务礼品", "旅行套装", "铁观音", "石敢当"];

  
  static SearchModel _instance = SearchModel._();
  factory SearchModel() => _instance;
  // 私有构造函数
  SearchModel._() {
    SharedPreferences.getInstance().then((prefs) {
      this._historyChips = prefs.getStringList("history_chips") ?? [];
      notifyListeners();
    });
  }

  /// 历史搜索关键词
  List<String> get historyChips {
    return _historyChips;
  }

  /// 猜你想搜关键词
  List<String> get guessChips {
    return _guessChips;
  }

  /// 清除历史关键词
  void clearHistoryChips() {
    SharedPreferences.getInstance().then((prefs) {
      this._historyChips = [];
      prefs.setStringList("history_chips", _historyChips);
      notifyListeners();
    });
  }

  /// 添加历史关键词
  void addHistoryChips(String keyword) {
    SharedPreferences.getInstance().then((prefs) {
      _historyChips.add(keyword);
      prefs.setStringList("history_chips", _historyChips);
      notifyListeners();
    }).catchError((err) {
      print(err);
    });
  }
}

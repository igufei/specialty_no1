import 'package:cached_network_image/cached_network_image.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/modules/utils.dart';
import 'package:specialty_no1/routes/routes.dart';
/// 分类页面右侧分类项展示
class RightTabView extends StatelessWidget {
  final int index;
  final List data;
  RightTabView({this.index = 0, this.data});
  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: index,
      children: data.map((item) {
        var children = item["children"] as List;
        return SingleChildScrollView(
          child: GridView.count(
            padding: EdgeInsets.only(top: 20, left: 20, right: 20),
            crossAxisCount: 3,
            childAspectRatio: 0.7,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: children.map((item) {
              return SizedBox(
                width: MySize.V(90),
                child: GestureDetector(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        width: MySize.V(90),
                        height: MySize.V(90),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          placeholder: (ctx, url) => Padding(padding: EdgeInsets.all(MySize.V(60)), child: CircularProgressIndicator()),
                          imageUrl: item["image"],
                          errorWidget: (ctx, url, err) => Image.asset('assets/images/error.png'),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          item["title"],
                          style: TextStyle(fontSize: MySize.S(16), color: Color(0xff58595b)),
                        ),
                      )
                    ],
                  ),
                  onTap: () => _tapItem(context, item["title"], item["keyword"]),
                ),
              );
            }).toList(),
          ),
        );
      }).toList(),
    );
  }

  void _tapItem(BuildContext ctx, String title, String keyword) {
    title = MyText.encode(title);
    keyword = MyText.encode(keyword);
    Application.router.navigateTo(ctx, "/list?title=$title&keyword=$keyword", transition: TransitionType.cupertino);
  }
}

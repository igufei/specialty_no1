import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/effect.dart';
/// 分类页面左侧导航
class LeftTabBar extends StatelessWidget {
  final List<String> items;
  final int currentIndex;
  final ValueChanged<int> onTap;

  LeftTabBar({
    this.currentIndex,
    this.onTap,
    @required this.items,
  });
  @override
  Widget build(BuildContext context) {
    var _bgColor = Color(0xfff5f5f5);
    return Container(
      width: MySize.V(130),
      decoration: BoxDecoration(
        color: _bgColor,
      ),
      child: ScrollConfiguration(
        behavior: OverScrollBehavior(),
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              child: SizedBox(
                height: MySize.V(60),
                child: Builder(
                  builder: (context) {
                    if (currentIndex == index) {
                      return Container(
                        //alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(vertical: MySize.V(18)),
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            border: Border(left: BorderSide(width: 3, color: MyColors.mainColor)),
                          ),
                          child: Text(
                            items[index],
                            style: TextStyle(color: MyColors.mainColor, fontWeight: FontWeight.w500, fontSize: MySize.S(18)),
                          ),
                        ),
                      );
                    }
                    return Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: _bgColor,
                        border: Border(left: BorderSide(width: 3, color: _bgColor)),
                      ),
                      child: Text(
                        items[index],
                        style: TextStyle(color: Color(0xff666666), fontWeight: FontWeight.w300, fontSize: MySize.S(18)),
                      ),
                    );
                  },
                ),
              ),
              onTap: () {
                if (onTap != null) onTap(index);
              },
            );
          },
        ),
      ),
    );
  }
}
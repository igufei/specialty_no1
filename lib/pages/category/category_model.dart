import 'package:flutter/cupertino.dart';
/// 分类页面数据处理
class CategoryModel extends ChangeNotifier {
  static CategoryModel _instance = CategoryModel._();
  factory CategoryModel() => _instance;
  int _currentIndex = 0;
  List _catData = [];
  CategoryModel._() {
    _request();
  }

  List get catData => _catData;

  int get currentIndex => _currentIndex;

  set currentIndex(int index) {
    _currentIndex = index;
    notifyListeners();
  }

  void _request() async {
    _catData = [
      {
        "channelID": "1000",
        "title": "地方特产",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "1001",
            "title": "湖南",
            "keyword": "特产 湖南",
            "image": "http://img.alicdn.com/imgextra/i4/2200692844799/O1CN01tG0T5I1lJzEIDDuaR_!!2200692844799.jpg",
          },
          {
            "channelID": "1002",
            "title": "云南",
            "keyword": "特产 云南",
            "image": "http://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i2/1854921980/O1CN01FORFyG1QUsmTN5rFf_!!1854921980.jpg",
          },
          {
            "channelID": "1003",
            "title": "东北",
            "keyword": "特产 东北",
            "image": "http://img.alicdn.com/imgextra/i1/2097000079/O1CN01fgEULM1CSDo8xuoGL_!!2097000079.jpg",
          },
          {
            "channelID": "1004",
            "title": "山东",
            "keyword": "特产 山东",
            "image": "http://gd2.alicdn.com/imgextra/i2/2064775210/TB2XOnivr4npuFjSZFmXXXl4FXa_!!2064775210.jpg",
          },
          {
            "channelID": "1005",
            "title": "广西",
            "keyword": "特产 广西",
            "image": "http://img.alicdn.com/imgextra/i2/4084823779/O1CN01AFUCsl1dmpCySxfQE_!!4084823779.jpg",
          },
          {
            "channelID": "1006",
            "title": "川渝",
            "keyword": "特产 川渝",
            "image": "http://img.alicdn.com/imgextra/i4/236996386/O1CN01ph3ItF1x2poxo3fkC_!!236996386.jpg",
          },
        ]
      },
      {
        "channelID": "2000",
        "title": "山珍海产",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "2001",
            "title": "海产干货",
            "keyword": "特产 海产干货",
            "image": "http://img.alicdn.com/imgextra/i1/353343116/O1CN01sLQ8bs1YtAe1NEakg_!!353343116.jpg",
          },
          {
            "channelID": "2002",
            "title": "山珍干货",
            "keyword": "特产 山珍干货",
            "image": "http://img.alicdn.com/imgextra/i4/725677994/O1CN01vXyrxT28vIi6qNDyH_!!725677994.jpg",
          },
          {
            "channelID": "2003",
            "title": "即食海味",
            "keyword": "特产 即食海味",
            "image": "http://gd4.alicdn.com/imgextra/i4/729329426/O1CN01iRJ6Zn2JV9t1ctQLc_!!729329426.jpg",
          }
        ]
      },
      {
        "channelID": "3000",
        "title": "茶叶茶具",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "3001",
            "title": "苦荞茶",
            "keyword": "特产 苦荞茶",
            "image": "http://gd4.alicdn.com/imgextra/i4/2647258344/TB2aWThyyCYBuNkSnaVXXcMsVXa_!!2647258344.jpg",
          },
          {
            "channelID": "3002",
            "title": "花茶",
            "keyword": "特产 花茶",
            "image": "http://gd4.alicdn.com/imgextra/i4/102566602/TB2sHS2mHGYBuNjy0FoXXciBFXa_!!102566602.jpg",
          },
          {
            "channelID": "3003",
            "title": "普洱茶",
            "keyword": "特产 普洱茶",
            "image": "http://gd3.alicdn.com/imgextra/i3/4127796298/O1CN01Yzvq491wOWxcvnO7r_!!4127796298.jpg",
          },
          {
            "channelID": "3004",
            "title": "碧螺春",
            "keyword": "特产 碧螺春",
            "image": "http://gd1.alicdn.com/imgextra/i1/150050791/O1CN01zSo99r1HiJnUGY7fz_!!150050791.jpg",
          },
          {
            "channelID": "3005",
            "title": "绿茶",
            "keyword": "特产 绿茶",
            "image": "http://gd4.alicdn.com/imgextra/i4/10477507/O1CN01dfpuLe25KFp2VsxrN_!!10477507.jpg",
          },
          {
            "channelID": "3006",
            "title": "红茶",
            "keyword": "特产 红茶",
            "image": "http://gd4.alicdn.com/imgextra/i4/3584443084/O1CN01pEXR2v1YeVy8VFiT3_!!3584443084.jpg",
          },
          {
            "channelID": "3007",
            "title": "茶具套件",
            "keyword": "特产 茶具套件",
            "image": "http://gd1.alicdn.com/imgextra/i1/200558061/T2ULW5XhBXXXXXXXXX_!!200558061.jpg",
          },
          {
            "channelID": "3008",
            "title": "茶宠/茶盘",
            "keyword": "特产 茶宠茶盘",
            "image": "http://gd1.alicdn.com/imgextra/i1/2239768215/O1CN012AYW8fUhNtrAgSU_!!2239768215.jpg",
          }
        ]
      },
      {
        "channelID": "4000",
        "title": "零食/冲调",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "4001",
            "title": "牛轧糖/糖果",
            "keyword": "特产 牛轧糖 糖果",
            "image": "http://gd1.alicdn.com/imgextra/i1/193384277/O1CN019W9opP1hSuWt7CbCT_!!193384277.jpg",
          },
          {
            "channelID": "4002",
            "title": "饼干/糕点",
            "keyword": "特产 饼干 糕点 膨化零嘴",
            "image": "https://gd2.alicdn.com/imgextra/i2/2221209286/O1CN01s2KYWS2IT2PX2wbDH_!!2221209286.jpg",
          },
          {
            "channelID": "4003",
            "title": "凤梨酥/麻薯",
            "keyword": "特产 凤梨酥 麻薯",
            "image": "https://gd4.alicdn.com/imgextra/i4/2101959732/O1CN012LlIy2VcOCZPKqD_!!2101959732.jpg",
          },
          {
            "channelID": "4004",
            "title": "蜜饯/果脯",
            "keyword": "特产 蜜饯 果脯",
            "image": "https://gd2.alicdn.com/imgextra/i2/1752809601/TB23JvIbBAOyuJjy0FlXXcaxFXa_!!1752809601.jpg",
          },
          {
            "channelID": "4005",
            "title": "坚果/炒货",
            "keyword": "特产 坚果 炒货",
            "image": "https://gd1.alicdn.com/imgextra/i1/4227124142/O1CN01EZUtwF1gT52xzrLnA_!!4227124142.jpg",
          },
          {
            "channelID": "4006",
            "title": "冲饮/奶茶",
            "keyword": "特产 冲饮 奶茶",
            "image": "http://img.alicdn.com/imgextra/i1/TB1T6M2OFXXXXbPapXXXXXXXXXX_!!0-item_pic.jpg",
          }
        ]
      },
      {
        "channelID": "5000",
        "title": "民族传统",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "5001",
            "title": "民族服装",
            "keyword": "民族服装",
            "image": "https://gd1.alicdn.com/imgextra/i1/810480145/O1CN011CwRsFhDMxd8b9G_!!810480145.jpg",
          },
          {
            "channelID": "5002",
            "title": "民族包包",
            "keyword": "民族包包",
            "image": "https://gd1.alicdn.com/imgextra/i1/67772917/O1CN01G10cyD1XQ1sfOeQFv_!!67772917.jpg",
          },
          {
            "channelID": "5003",
            "title": "民族饰品",
            "keyword": "民族饰品",
            "image": "https://gd3.alicdn.com/imgextra/i3/831911963/O1CN01WJZARu1QN5zJkhrnn_!!831911963.jpg",
          },
          {
            "channelID": "5004",
            "title": "工艺摆件",
            "keyword": "特产 工艺摆件",
            "image": "https://gd4.alicdn.com/imgextra/i4/117239468/TB2kbSDfdHO8KJjSZFLXXaTqVXa_!!117239468.jpg",
          },
          {
            "channelID": "5005",
            "title": "手工绣品",
            "keyword": "特产 手工绣品",
            "image": "https://gd4.alicdn.com/imgextra/i4/1868471360/TB2WKFuennI8KJjy0FfXXcdoVXa_!!1868471360.jpg",
          },
          {
            "channelID": "5006",
            "title": "手工布鞋",
            "keyword": "手工布鞋",
            "image": "https://gd1.alicdn.com/imgextra/i1/2402732569/O1CN01ObePD41Uqe1n24rPm_!!2402732569.png",
          },
          {
            "channelID": "5007",
            "title": "手工木雕",
            "keyword": "手工木雕",
            "image": "https://gd3.alicdn.com/imgextra/i3/90152540/TB2yKeFeN9YBuNjy0FfXXXIsVXa_!!90152540.jpg",
          },
          {
            "channelID": "5008",
            "title": "小物件",
            "keyword": "特产 民族特色",
            "image": "http://img.alicdn.com/imgextra/i2/2201805967448/O1CN01ZH0W5K24tES5YkHCc_!!2201805967448.jpg",
          }
        ]
      },
      {
        "channelID": "6000",
        "title": "优质生鲜",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "6001",
            "title": "鸡蛋",
            "keyword": "草鸡蛋",
            "image": "https://gd4.alicdn.com/imgextra/i4/2269058871/TB2qgAAzVuWBuNjSszbXXcS7FXa_!!2269058871.jpg",
          },
          {
            "channelID": "6002",
            "title": "草鸡",
            "keyword": "草鸡",
            "image": "https://gd1.alicdn.com/imgextra/i1/260545882/TB2MH77XA1M.eBjSZFFXXc3vVXa_!!260545882.jpg",
          },
          {
            "channelID": "6003",
            "title": "老鸭",
            "keyword": "老鸭",
            "image": "http://img.alicdn.com/imgextra/i3/3236345693/O1CN01hWqRcT1rvRJ1bNa7r_!!3236345693.png",
          },
          {
            "channelID": "6004",
            "title": "大闸蟹",
            "keyword": "特产 鲜活大闸蟹",
            "image": "http://img.alicdn.com/imgextra/i4/2201513990119/O1CN01DbS4wn1CkXgkOY64h_!!2201513990119.jpg",
          },
          {
            "channelID": "6005",
            "title": "水果",
            "keyword": "特产 水果",
            "image": "http://img.alicdn.com/imgextra/i4/2201257922128/O1CN01xrMei41RafNnqdZTp_!!2201257922128.jpg",
          },
          {
            "channelID": "6006",
            "title": "蔬菜",
            "keyword": "特产 蔬菜",
            "image": "https://gd3.alicdn.com/imgextra/i3/765013941/TB2J_6FewoQMeJjy0FnXXb8gFXa_!!765013941.jpg",
          },
        ]
      },
      {
        "channelID": "7000",
        "title": "养身滋补",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "7001",
            "title": "孕期滋补",
            "keyword": "特产 孕期滋补",
            "image": "https://gd4.alicdn.com/imgextra/i4/667183974/O1CN01s7zHRz1fE8WWEUm4u_!!667183974.jpg",
          },
          {
            "channelID": "7002",
            "title": "强健男人",
            "keyword": "特产 鞭",
            "image": "https://gd1.alicdn.com/imgextra/i1/405895333/TB2.0fJtRyWBuNkSmFPXXXguVXa_!!405895333.jpg",
          },
          {
            "channelID": "7003",
            "title": "中药材",
            "keyword": "特产 中药材",
            "image": "https://gd3.alicdn.com/imgextra/i3/3288480982/TB2Z8oJjLuSBuNkHFqDXXXfhVXa_!!3288480982.jpg",
          }
        ]
      },
      {
        "channelID": "8000",
        "title": "肉干/肉食",
        "keyword": "",
        "image": "",
        "children": [
          {
            "channelID": "8001",
            "title": "猪肉干",
            "keyword": "特产 猪肉干",
            "image": "https://gd1.alicdn.com/imgextra/i1/389870112/O1CN01LjzwlJ1ChKqa8KMd8_!!389870112.jpg",
          },
          {
            "channelID": "8002",
            "title": "牛肉干",
            "keyword": "特产 牛肉干",
            "image": "http://img.alicdn.com/imgextra/i2/793045623/O1CN01Wye4r01rPNbtbA9Vy_!!793045623.jpg",
          },
          {
            "channelID": "8003",
            "title": "肉粽",
            "keyword": "特产 肉粽",
            "image": "http://img.alicdn.com/imgextra/i1/2200723428610/O1CN01cJo2cH2DTQdQqJdmc_!!2200723428610.jpg",
          },
          {
            "channelID": "8004",
            "title": "火腿",
            "keyword": "特产 火腿",
            "image": "http://img.alicdn.com/imgextra/i1/3261281956/O1CN011QJt9dss4ytzFVo_!!3261281956.jpg",
          },
        ]
      },
    ];
    return await Future<List>.delayed(Duration(milliseconds: 1000)).then((List value) {
      notifyListeners();
    });
  }
}

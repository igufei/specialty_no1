import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/category/category_model.dart';
import 'package:specialty_no1/pages/category/widgets/left_tab_bar.dart';
import 'package:specialty_no1/pages/category/widgets/right_tab_view.dart';
import 'package:specialty_no1/widgets/search_bar_button.dart';
/// 分类页面
class CategoryPage extends StatelessWidget {
  final _model = CategoryModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(MySize.appBarHeight),
        child: AppBar(
          title: SearchBarButton(),
          centerTitle: true,
          titleSpacing: 0,
        ),
      ),
      body: MultiProvider(
        providers: [ChangeNotifierProvider.value(value: _model)],
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Consumer<CategoryModel>(
              builder: (context, model, widget) => LeftTabBar(
                currentIndex: _model.currentIndex,
                items: _model.catData.map((item) => item["title"].toString()).toList(),
                onTap: (index) => _model.currentIndex = index,
              ),
            ),
            Consumer<CategoryModel>(
              builder: (context, model, widget) => Expanded(
                child: RightTabView(
                  index: _model.currentIndex,
                  data: _model.catData,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
/// 通知页面
class NoticePage extends StatelessWidget {
  final String id;
  NoticePage({this.id});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("通知"),
        centerTitle: true,
      ),
      body: Container(
        child: Text(this.id),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';
/// 商品列表页面顶部导航栏
class TopBar extends StatefulWidget {
  final List tabs;
  final Function(int index) onTap;
  TopBar({
    @required this.tabs,
    this.onTap,
  });
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MySize.V(50),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(bottom: BorderSide(color: Colors.grey[200])),
      ),
      child: Row(
        children: widget.tabs
            .asMap()
            .keys
            .map(
              (index) => Expanded(
                child: InkWell(
                  child: Center(
                    child: Text(
                      widget.tabs[index]["title"],
                      style: _currentIndex == index
                          ? TextStyle(fontSize: MySize.S(19), color: MyColors.mainColor)
                          : TextStyle(fontSize: MySize.S(19), color: Colors.black54),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      _currentIndex = index;
                    });
                    widget.onTap(index);
                  },
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}

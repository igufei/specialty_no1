import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/goods/goods_item.dart';
import 'package:specialty_no1/widgets/icons.dart';
/// 布局切换按钮，通常放置在顶部导航栏右侧
class SwichButton extends StatefulWidget {
  final void Function(LayoutType) onTap;
  final LayoutType layoutType;
  const SwichButton({
    Key key,
    this.onTap,
    @required this.layoutType,
  }) : super(key: key);
  @override
  _SwichButtonState createState() => _SwichButtonState();
}

class _SwichButtonState extends State<SwichButton> {
  LayoutType _layoutType;
  @override
  void initState() {
    _layoutType = widget.layoutType;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: SizedBox(
        child: _getIconWidget(),
      ),
      onTap: () => _tap(),
    );
  }

  Widget _getIconWidget() {
    if (_layoutType == LayoutType.Grid) {
      return Icon(MyIcons.list1, size: MySize.S(35));
    } else if (_layoutType == LayoutType.Column) {
      return Icon(MyIcons.list2, size: MySize.S(33));
    } else {
      return Container();
    }
  }

  void _tap() {
    setState(() {
      if (widget.layoutType == LayoutType.Grid) {
        _layoutType = LayoutType.Column;
      } else if (widget.layoutType == LayoutType.Column) {
        _layoutType = LayoutType.Grid;
      } else {
        _layoutType = LayoutType.Column;
      }
      widget.onTap(_layoutType);
    });
  }
}

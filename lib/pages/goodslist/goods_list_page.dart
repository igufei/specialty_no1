import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/models/layout_value.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/goodslist/widgets/swich_button.dart';
import 'package:specialty_no1/pages/goodslist/widgets/top_bar.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/goods/goods_item.dart';
import 'package:specialty_no1/widgets/goods/goods_view.dart';
import 'package:specialty_no1/widgets/goods/goods_model.dart';

/// 商品列表排列类型
enum PageType {
  /// 分类展示
  Category,

  /// 搜索展示
  Search,

  /// 推荐展示
  Recommended,
}
/// 商品列表展示页面
class GoodsListPage extends StatelessWidget {
  final ScrollController _scrollController = ScrollController();
  final String title;
  final String keyword;
  final LayoutType layoutType;
  final GoodsModel _model = GoodsModel();
  GoodsListPage({
    this.title = "",
    this.keyword,
    this.layoutType = LayoutType.Grid,
  }) {
    _model.setValue(keyword: keyword);
  }
  final _tabs = [
    {
      "title": "综合",
    },
    {
      "title": "销量",
    },
    {
      "title": "价格",
    },
    /* {
      "title": "筛选",
    } */
  ];
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: _model)],
      child: Scaffold(
        backgroundColor: MyColors.bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(MySize.V(60)),
          child: AppBar(
            title: Text(
              title,
              style: TextStyle(fontSize: MySize.appBarTitleFontSize),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: Consumer<LayoutValue>(
                  builder: (context, LayoutValue value, widget) {
                    return SwichButton(
                      layoutType: value.layoutType,
                      onTap: (layoutType) {
                        value.layoutType = layoutType;
                      },
                    );
                  },
                ),
              )
            ],
            centerTitle: true,
          ),
        ),
        body: Stack(children: [
          Container(
            margin: EdgeInsets.only(top: MySize.V(50)),
            child: Consumer2<GoodsModel, LayoutValue>(
              builder: (context, model, LayoutValue value, widget) => GoodsView(
                scrollController: _scrollController,
                model: _model,
                layoutType: value.layoutType,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: TopBar(
              tabs: _tabs,
              onTap: (index) => _tapTabItem(index),
            ),
          ),
        ]),
      ),
    );
  }

  /// 点击tab项
  void _tapTabItem(int index) async {
    _scrollController.jumpTo(0);
    _model.setValue(sort: index + 1);
  }
}

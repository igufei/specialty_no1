import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';

/// 404 错误页面
class ErrorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("错误"),
      ),
      body: Center(
        child: Text(
          "页面没有找到",
          style: TextStyle(fontSize: MySize.S(60)),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/category/category_page.dart';
import 'package:specialty_no1/pages/index/index_page.dart';
import 'package:specialty_no1/pages/personal/personal_page.dart';
import 'package:specialty_no1/pages/recommended/recommended_page.dart';
import 'package:specialty_no1/pages/shoppingcart/shopping_cart_page.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/icons.dart';

/// 所有页面的主框架入口
class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  // 当前激活的页面页码
  int _currentIndex = 0;
  // 底部按钮数据
  List _navItems = [
    {
      "title": "首页",
      "icon": MyIcons.home,
      "iconSize": 40.0,
    },
    {
      "title": "分类",
      "icon": MyIcons.classification,
      "iconSize": 36.0,
    },
    {
      "title": "推荐",
      "icon": MyIcons.recommended,
      "iconSize": 44.0,
    },
    {
      "title": "购物车",
      "icon": MyIcons.shoppingCart,
      "iconSize": 40.0,
    },
    {
      "title": "我的",
      "icon": MyIcons.my,
      "iconSize": 36.0,
    },
  ];

  List<Widget> _pages = [
    // 主页
    IndexPage(),
    // 分类页面
    CategoryPage(),
    // 推荐页面
    RecommendedPage(),
    // 购物车页面
    ShoppingCartPage(),
    // 个人中心页面
    PersonalPage(),
  ];

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 540, height: 960)..init(context);
    return Scaffold(
      backgroundColor: MyColors.bgColor,
      bottomNavigationBar: SizedBox(
        child: BottomNavigationBar(
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          fixedColor: Theme.of(context).accentColor,
          selectedFontSize: MySize.S(16),
          unselectedFontSize: MySize.S(16),
          items: _navItems.map((item) {
            return BottomNavigationBarItem(
              title: Text(item["title"]),
              icon: SizedBox(
                width: MySize.V(44),
                height: MySize.V(44),
                child: Icon(
                  item["icon"],
                  size: MySize.S(item["iconSize"]),
                ),
              ),
            );
          }).toList(),
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: _pages,
      ),
    );
  }
}

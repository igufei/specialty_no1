import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/icons.dart';

class EmptyCartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 30),
            alignment: Alignment.center,
            height: MySize.S(140),
            width: MySize.S(140),
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(70),
            ),
            child: ClipRRect(
              child: Icon(
                MyIcons.shoppingCart,
                size: MySize.S(100),
                color: Colors.white,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20,bottom: 20),
            child: Text(
              "~购物车还是空的呢",
              style: TextStyle(fontSize: MySize.S(24), color: MyColors.gray),
            ),
          )
        ],
      ),
    );
  }
}

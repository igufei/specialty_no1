import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:specialty_no1/models/keyword.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/index/widgets/guess_U_like.dart';
import 'package:specialty_no1/pages/shoppingcart/widgets/empty_cart.dart';
import 'package:specialty_no1/pages/shoppingcart/widgets/shopping_cart_item_widget.dart';
import 'package:specialty_no1/widgets/goods/goods_model.dart';
import 'package:specialty_no1/widgets/my_list_view.dart';

/// 购物车页面
class ShoppingCartPage extends StatelessWidget {
  final List data = [];
  final GoodsModel _goodsModel = GoodsModel();
  ShoppingCartPage() {
    _goodsModel.setValue(keyword: Keyword.guessULike);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  Widget _appBar() {
    return PreferredSize(
      child: AppBar(
        title: Text(
          "购物车",
          style: TextStyle(
            fontSize: MySize.appBarTitleFontSize,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          GestureDetector(
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 10),
              child: Text(
                "管理",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: MySize.S(22),
                ),
              ),
            ),
            onTap: () {},
          )
        ],
      ),
      preferredSize: Size.fromHeight(MySize.appBarHeight),
    );
  }

  Widget _body() {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: _goodsModel)],
      child: Container(
        color: Color(0xffeeeeee),
        child: MyListView(
          onLoading: _onLoading,
          enablePullDown: false,
          children: <Widget>[
            Builder(
              builder: (context) {
                if (data.length == 0) {
                  return EmptyCartWidget();
                } else {
                  return ListView(
                    children: <Widget>[
                      ShoppingCartItemWidget(),
                    ],
                  );
                }
              },
            ),
            Consumer<GoodsModel>(
              builder: (context, model, widget) => GuessULike(goodsList: _goodsModel.data),
            ),
          ],
        ),
      ),
    );
  }

  /// 下拉加载数据
  void _onLoading(RefreshController rc) async {
    await _goodsModel.setValue(pageNum: _goodsModel.pageNum + 1);
    rc.loadComplete();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:specialty_no1/models/keyword.dart';
import 'package:specialty_no1/modules/http.dart';

class IndexModel extends ChangeNotifier {
  List _swipers = [
    {
      "image": "http://m.360buyimg.com/img/jfs/t1/99614/27/656/84852/5db1409dE6f83c84f/6991ce23e23e1cf0.jpg",
      "data": {
        "auctionId": "1",
      },
    },
    {
      "image": "http://img14.360buyimg.com/img/jfs/t1/102117/22/574/112931/5dafaa50E5b884c2d/153ef01f9f777696.jpg",
      "data": {
        "auctionId": "2",
      },
    },
    {
      "image": "http://img10.360buyimg.com/img/jfs/t1/57433/37/13988/76424/5db14016E36b68960/04d91703b0e8976e.jpg",
      "data": {
        "auctionId": "",
      },
    },
  ];
  List _headerMenus = [
    {
      "id": "0",
      "title": "广东",
      "image": "assets/images/wan.png",
    },
    {
      "id": "1",
      "title": "四川",
      "image": "assets/images/cj.png",
    },
    {
      "id": "2",
      "title": "福建",
      "image": "assets/images/dg.png",
    },
    {
      "id": "3",
      "title": "云南",
      "image": "assets/images/xc.png",
    },
    {
      "id": "4",
      "title": "全部",
      "image": "assets/images/more.png",
    },
  ];
  List _greatGoods = [];
  List _timeBuyGoods = [];
  List _notices = [];
  static IndexModel _instance = IndexModel._();
  factory IndexModel() => _instance;
  IndexModel._() {
    _request();
  }

  /// 轮播图数据
  get swipers => _swipers;

  /// 头部菜单数据
  get headerMenus => _headerMenus;

  /// 有好货商品数据
  get greatGoods => _greatGoods;

  /// 限时抢购商品数据
  get timeBuyGoods => _timeBuyGoods;

  /// 通知数据
  get notices => _notices;
  // 请求数据
  Future<void> _request() async {
    _swipers = [
      {
        "image": "http://m.360buyimg.com/img/jfs/t1/99614/27/656/84852/5db1409dE6f83c84f/6991ce23e23e1cf0.jpg",
        "data": {
          "auctionId": "1",
        },
      },
      {
        "image": "http://img14.360buyimg.com/img/jfs/t1/102117/22/574/112931/5dafaa50E5b884c2d/153ef01f9f777696.jpg",
        "data": {
          "auctionId": "2",
        },
      },
      {
        "image": "http://img10.360buyimg.com/img/jfs/t1/57433/37/13988/76424/5db14016E36b68960/04d91703b0e8976e.jpg",
        "data": {
          "auctionId": "",
        },
      },
    ];
    _headerMenus = [
      {
        "id": "0",
        "title": "广东",
        "keyword": "特产 广东",
        "image": "assets/images/wan.png",
      },
      {
        "id": "1",
        "title": "四川",
        "keyword": "特产 四川",
        "image": "assets/images/cj.png",
      },
      {
        "id": "2",
        "title": "福建",
        "keyword": "特产 福建",
        "image": "assets/images/dg.png",
      },
      {
        "id": "3",
        "title": "云南",
        "keyword": "特产 云南",
        "image": "assets/images/xc.png",
      },
      {
        "id": "4",
        "title": "全部",
        "keyword": "特产 地方",
        "image": "assets/images/more.png",
      },
    ];
    _notices = [
      {
        "id": 0,
        "tag": "公告",
        "title": "近期订单配送延迟公告",
      },
      {
        "id": 1,
        "tag": "新品特价",
        "title": "50元券限量抢",
      },
      {
        "id": 2,
        "tag": "银行精选",
        "title": "限时加息 最高+1.2%",
      },
    ];
    var res1 = await http.post('/goods/search', data: {"q": Keyword.timeBuy});
    var res2 = await http.post('/goods/search', data: {"q": Keyword.greatGoods});
    _timeBuyGoods = res1.data;
    _greatGoods = res2.data;
    notifyListeners();
  }

  /// 刷新数据
  Future<void> refresh() async {
    await _request();
  }
}

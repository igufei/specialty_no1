import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:specialty_no1/models/keyword.dart';
import 'package:specialty_no1/models/opacity_value.dart';
import 'package:specialty_no1/pages/index/index_model.dart';
import 'package:specialty_no1/pages/index/widgets/cabinet_view.dart';
import 'package:specialty_no1/pages/index/widgets/guess_U_like.dart';
import 'package:specialty_no1/pages/index/widgets/header_menu.dart';
import 'package:specialty_no1/pages/index/widgets/new_user_channel.dart';
import 'package:specialty_no1/pages/index/widgets/swiper_view.dart';
import 'package:specialty_no1/pages/index/widgets/top_status_bar.dart';
import 'package:specialty_no1/widgets/goods/goods_model.dart';
import 'package:specialty_no1/widgets/my_list_view.dart';
/// 首页展示
class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  final IndexModel _indexModel = IndexModel();
  final GoodsModel _goodsModel = GoodsModel();
  final ScrollController _scrollController = ScrollController();
  final RefreshController _refreshController = RefreshController(initialRefresh: true);
  _IndexPageState() {
    _scrollController.addListener(() {
      Provider.of<TopStatusBarOpacityValue>(context).setValue(_scrollController.offset);
    });
  }
  @override
  void initState() {
    super.initState();
    _goodsModel.setValue(keyword: Keyword.guessULike);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: _indexModel),
        ChangeNotifierProvider.value(value: _goodsModel),
      ],
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Consumer2<IndexModel, GoodsModel>(
              builder: (context, indexModel, goodsModel, widget) => MyListView(
                controller: _scrollController,
                header: MaterialClassicHeader(),
                padding: EdgeInsets.all(0),
                children: <Widget>[
                  // 轮播图
                  SwiperView(swiperList: _indexModel.swipers),

                  // 分类导航
                  HeaderMenu(items: _indexModel.headerMenus),

                  // 新用户通道
                  NewUserChannel(),

                  // 橱窗展示
                  CabinetView(
                    greatGoods: _indexModel.greatGoods,
                    timeBuyGoods: _indexModel.timeBuyGoods,
                    notices: _indexModel.notices,
                  ),

                  // 猜你喜欢
                  GuessULike(goodsList: _goodsModel.data),
                ],
                onRefresh: _onRefresh,
                onLoading: _onLoading,
              ),
            ),
            TopStatusBar()
          ],
        ),
      ),
    );
  }

  /// 上拉刷新数据
  void _onRefresh(RefreshController rc) async {
    Provider.of<TopStatusBarOpacityValue>(context).setVisible(false);
    await _indexModel.refresh();
    await _goodsModel.refresh();
    rc.refreshCompleted();
    await Future.delayed(Duration(milliseconds: 500));
    Provider.of<TopStatusBarOpacityValue>(context).setVisible(true);
  }

  /// 下拉加载数据
  void _onLoading(RefreshController rc) async {
    await _goodsModel.setValue(pageNum: _goodsModel.pageNum + 1);
    rc.loadComplete();
  }
}

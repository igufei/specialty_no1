/// 轮播图
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:specialty_no1/routes/routes.dart';

class SwiperView extends StatelessWidget {
  final List<dynamic> swiperList;
  SwiperView({Key key, this.swiperList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width / 1.8 * 1,
      margin: EdgeInsets.only(bottom: 10),
      child: Swiper(
        autoplayDelay: 4000,
        itemCount: swiperList.length,
        itemBuilder: (context, index) {
          return Image.network(swiperList[index]["image"], fit: BoxFit.fill);
        },
        pagination: SwiperPagination(builder: DotSwiperPaginationBuilder(color: Colors.black54, activeColor: Colors.white)),
        controller: SwiperController(),
        scrollDirection: Axis.horizontal,
        autoplay: true,
        onTap: (index) {
          _clickSwiperItem(context, index);
        },
      ),
    );
  }

  // 点击轮播图项
  void _clickSwiperItem(BuildContext context, int index) {
    final String path = "/goods?auctionId=${swiperList[index]['data']['auctionId']}";
    Application.router.navigateTo(context, path, transition: TransitionType.cupertino);
  }
}


import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_model.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/countdown.dart';
import 'package:specialty_no1/widgets/effect.dart';
// 橱窗展示
class CabinetView extends StatefulWidget {
  final List greatGoods;
  final List timeBuyGoods;
  final List notices;

  const CabinetView({
    Key key,
    @required this.greatGoods,
    @required this.timeBuyGoods,
    @required this.notices,
  }) : super(key: key);
  @override
  _CabinetViewState createState() => _CabinetViewState();
}

class _CabinetViewState extends State<CabinetView> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 0, left: 10, right: 10, top: 0),
      elevation: 0.1,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: Container(
        child: Column(
          children: <Widget>[
            // 上边
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  // 头条ico
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        right: BorderSide(color: Color(0xfff2f2f2), width: 0.8),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(right: 8),
                      child: Image.asset(
                        "assets/images/msg.png",
                        height: MySize.V(20),
                      ),
                    ),
                  ),
                  // 内容
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 5),
                      height: MySize.V(30),
                      child: Swiper(
                        itemCount: widget.notices.length,
                        itemBuilder: (context, index) {
                          return widgetMsg(
                            widget.notices[index]["tag"],
                            widget.notices[index]["title"],
                          );
                        },
                        pagination: null,
                        controller: SwiperController(),
                        scrollDirection: Axis.vertical,
                        autoplay: true,
                        onTap: (index) => _tapNoticeItem(context, ""),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Divider(height: 0, thickness: 0),
            // 下面
            SizedBox(
              height: MySize.V(200),
              child: Row(
                children: <Widget>[
                  // 左
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        child: Column(
                          children: <Widget>[
                            //限时秒杀
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Image.asset(
                                      "assets/images/ms.png",
                                      height: MySize.V(22),
                                    ),
                                  ),
                                  /* Container(
                                    padding: EdgeInsets.symmetric(horizontal: 3, vertical: 1),
                                    child: Text(
                                      "02:39:04",
                                      style: TextStyle(fontSize: MySize.S(18), color: Color(0xffF20C59)),
                                    ),
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Color(0xffF20C59))),
                                  ) */
                                  CountdownWidget(
                                    endTime: TimeBuyModel().currentActivityEndTime,
                                    style: CountdownStyle.style2,
                                  ),
                                ],
                              ),
                            ),
                            // 秒杀列表
                            Expanded(
                              child: ScrollConfiguration(
                                behavior: OverScrollBehavior(),
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  children: widget.timeBuyGoods.map((item) {
                                    return Container(
                                      padding: EdgeInsets.all(4),
                                      child: Column(
                                        children: <Widget>[
                                          Image.network(item["pictUrl"], width: MySize.V(73)),
                                          Container(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Text(
                                              "¥" + item["zkPrice"].toString(),
                                              style: TextStyle(fontSize: MySize.S(18)),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              "¥" + item["zkPrice"].toString(),
                                              style: TextStyle(fontSize: MySize.S(18), color: Color(0xff919599), decoration: TextDecoration.lineThrough),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () => _tapTimeBuyView(context),
                    ),
                  ),
                  VerticalDivider(width: 0, thickness: 0),
                  // 右
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        color: Colors.transparent,
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text("值得买", style: TextStyle(fontSize: MySize.S(25), color: Color(0xfffd851d))),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text("品质好物 精心挑选", style: TextStyle(fontSize: MySize.S(16), color: Color(0xff666666))),
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.only(top: 5),
                                //width: MySize.V(270),
                                //height: MySize.V(110),
                                child: ScrollConfiguration(
                                  behavior: OverScrollBehavior(),
                                  child: ListView(
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    children: widget.greatGoods.map((item) {
                                      return Container(
                                        padding: EdgeInsets.only(top: 5, left: 22),
                                        child: Column(
                                          children: <Widget>[
                                            Image.network(item["pictUrl"], width: MySize.V(78)),
                                            Container(
                                              padding: EdgeInsets.only(top: 5),
                                              child: Text(
                                                "¥ " + item["zkPrice"].toString(),
                                                style: TextStyle(fontSize: MySize.S(18)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: () => _tapGreatGoodsView(context),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget widgetMsg(String tig, String title) {
    return Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 4),
          child: Text(
            tig,
            style: TextStyle(color: Colors.red, fontSize: MySize.S(16)),
          ),
          decoration: BoxDecoration(color: Color(0xffffdce7), borderRadius: BorderRadius.circular(5)),
        ),
        Padding(
          padding: EdgeInsets.only(left: 5),
          child: Text(
            title,
            style: TextStyle(fontSize: MySize.S(18), color: Color(0xff666666)),
          ),
        )
      ],
    );
  }

  void _tapNoticeItem(BuildContext context, String id) {
    Application.router.navigateTo(context, "/notice?id=$id", transition: TransitionType.cupertino);
  }

  void _tapTimeBuyView(BuildContext context) {
    Application.router.navigateTo(context, "/timebuy", transition: TransitionType.cupertino);
  }

  void _tapGreatGoodsView(BuildContext context) {
    Application.router.navigateTo(context, "/recommended?tabIndex=2", transition: TransitionType.cupertino);
  }
}


import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/modules/utils.dart';
import 'package:specialty_no1/routes/routes.dart';
/// ico分类按钮组
class HeaderMenu extends StatelessWidget {
  final List items;
  HeaderMenu({this.items});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: items.map((item) {
          return GestureDetector(
            child: SizedBox(
              //width: 65,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    item["image"],
                    width: MySize.V(60),
                    height: MySize.V(60),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(
                      item["title"],
                      style: TextStyle(
                        fontSize: MySize.S(18),
                        color: Color(0xff58595b),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            onTap: () {
              _tapMenuItem(context, item["title"], item["keyword"]);
            },
          );
        }).toList(),
      ),
    );
  }

  void _tapMenuItem(BuildContext ctx, String title, String keyword) {
    title = MyText.encode(title);
    keyword = MyText.encode(keyword);
    Application.router.navigateTo(ctx, "/list?title=$title&keyword=$keyword", transition: TransitionType.cupertino);
  }
}

import 'package:flutter/material.dart';
/// 新用户频道
class NewUserChannel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              child: Image.asset("assets/images/newuser1.gif"),
              onTap: () {},
            ),
          ),
          Expanded(
            child: GestureDetector(
              child: Image.asset("assets/images/newuser2.gif"),
              onTap: () {},
            ),
          ),
        ],
      ),
    );
  }
}

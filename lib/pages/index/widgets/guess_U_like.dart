import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/goods/goods_item.dart';
/// 猜你喜欢
class GuessULike extends StatelessWidget {
  final List goodsList;
  GuessULike({@required this.goodsList}) ;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top:10,bottom: 10, left: 10, right: 10),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 10),
            child: Image.asset(
              "assets/images/like.png",
              height: MySize.V(20),
            ),
          ),
          Wrap(
            spacing: MySize.V(10),
            runSpacing: MySize.V(10),
            alignment: WrapAlignment.center,
            children: this.goodsList.map((item) {
              return GoodsItem(goodsData: item);
            }).toList(),
          )
        ],
      ),
    );
  }
}

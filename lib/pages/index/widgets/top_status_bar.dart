
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/models/opacity_value.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/icons.dart';
import 'package:specialty_no1/widgets/search_bar_button.dart';
/// 顶部状态栏
class TopStatusBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<TopStatusBarOpacityValue>(
      builder: (context, TopStatusBarOpacityValue opacity, _) {
        if (opacity.value < 0 || !opacity.visible) {
          return Container();
        }
        double _headBarOpacity = opacity.value / 100.0 < 1.0 ? opacity.value / 100.0 : 1;
        return Container(
          color: Color.fromRGBO(MyColors.mainColor.red, MyColors.mainColor.green, MyColors.mainColor.blue, _headBarOpacity),
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top, left: 10, right: 10),
          child: Container(
            alignment: Alignment.center,
            height: MySize.V(60),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                /* Text(
                  "特产1号",
                  style: TextStyle(color: Colors.white, fontSize: MySize.S(24), fontWeight: FontWeight.w600),
                ), */
                SizedBox(
                  width: MySize.V(90),
                  child: Image.asset("assets/images/logo.png")),
                Expanded(child: SearchBarButton()),
                // 搜索button
                GestureDetector(
                  child: Icon(
                    MyIcons.classification,
                    size: MySize.S(34),
                    color: Colors.white,
                  ),
                  onTap: () {
                    _tapCategoryButton(context);
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }

  // 点击分类按钮
  void _tapCategoryButton(BuildContext context) {
    Application.router.navigateTo(context, "/category", transition: TransitionType.cupertino);
  }
}

import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';

class MyOrderWidget extends StatefulWidget {
  @override
  _MyOrderWidgetState createState() => _MyOrderWidgetState();
}

class _MyOrderWidgetState extends State<MyOrderWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          _firstTile(),
          Divider(height: 0, thickness: 0),
          _secondTile(),
        ],
      ),
    );
  }

  Widget _firstTile() {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          children: <Widget>[
            Text(
              "我的订单",
              style: TextStyle(color: MyColors.textColor, fontSize: MySize.S(22)),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    "查看全部",
                    style: TextStyle(
                      color: Color(0xff595858),
                      fontSize: MySize.S(19),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Icon(
                      Icons.chevron_right,
                      size: MySize.S(34),
                      color: Color(0xff9c9c9c),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _secondTile() {
    List items = [
      {
        "image": "assets/images/fk.png",
        "title": "待付款",
      },
      {
        "image": "assets/images/fh.png",
        "title": "待发货",
      },
      {
        "image": "assets/images/sh.png",
        "title": "待收货",
      },
      {
        "image": "assets/images/pj.png",
        "title": "待评价",
      },
      {
        "image": "assets/images/th.png",
        "title": "退款/售后",
      }
    ];
    return Padding(
      padding: EdgeInsets.only(left: 10,right: 10, top: 15,bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: items
            .map((item) => Container(
                  width: MySize.V(100),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          item["image"],
                          height: MySize.V(30),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                          item["title"],
                          style: TextStyle(fontSize: MySize.S(18), color: MyColors.textColor),
                        ),
                      ),
                    ],
                  ),
                ))
            .toList(),
      ),
    );
  }
}

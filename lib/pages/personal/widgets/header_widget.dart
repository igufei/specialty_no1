import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/icons.dart';

class HeaderWidget extends StatefulWidget {
  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: MySize.statusBarHeight),
      color: MyColors.mainColor,
      child: Container(
        //height: MySize.V(50),
        child: Column(
          children: <Widget>[
            _titleTile(),
            _nickTile(),
          ],
        ),
      ),
    );
  }

  // 标题
  Widget _titleTile() {
    return Row(
      children: <Widget>[
        Expanded(child: Container()),
        Expanded(
          child: Center(
            child: Text(
              "个人中心",
              style: TextStyle(
                fontSize: MySize.S(30),
                color: Colors.white,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(right: 8),
            child: GestureDetector(
              child: Text(
                "设置",
                style: TextStyle(
                  fontSize: MySize.S(22),
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  // 用户名
  Widget _nickTile() {
    return Container(
      height: MySize.V(110),
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: MySize.V(60),
                width: MySize.V(60),
                child: Icon(
                  MyIcons.my,
                  color: MyColors.gray,
                  size: MySize.V(40),
                ),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(30),
                color: Color(0xfff6f6f6),
                ),
                
              ),
              Padding(
                padding: EdgeInsets.only(left: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "IGF",
                        style: TextStyle(fontSize: MySize.S(34), color: Colors.white),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "资料、收货地址 >",
                        style: TextStyle(fontSize: MySize.S(20), color: Colors.white),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Container(
            alignment: Alignment.center,
            height: MySize.V(36),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(18)),
            child: Text(
              "会员中心 >",
              style: TextStyle(fontSize: MySize.S(20), color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}

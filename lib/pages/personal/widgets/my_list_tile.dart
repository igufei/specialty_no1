
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';

class MyListTile extends StatefulWidget {
  final String title;
  final bool hasDivider;
  const MyListTile({
    Key key,
    this.title = "",
    this.hasDivider = false,
  }) : super(key: key);
  @override
  _MyListTileState createState() => _MyListTileState();
}

class _MyListTileState extends State<MyListTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              children: <Widget>[
                Text(
                  widget.title,
                  style: TextStyle(color: MyColors.textColor, fontSize: MySize.S(22)),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 0),
                        child: Icon(
                          Icons.chevron_right,
                          size: MySize.S(34),
                          color: Color(0xff9c9c9c),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Builder(
            builder: (context) {
              if (widget.hasDivider) {
                return Divider(
                  height: 0,
                  thickness: 0.5,
                  //color: Color(0xfff6f6f6),
                  indent: MySize.V(15),
                );
              }
              return Container();
            },
          )
        ],
      ),
    );
  }
}

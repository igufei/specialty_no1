import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/pages/personal/widgets/header_widget.dart';
import 'package:specialty_no1/pages/personal/widgets/my_list_tile.dart';
import 'package:specialty_no1/pages/personal/widgets/my_order_widget.dart';
import 'package:specialty_no1/widgets/my_list_view.dart';

/// 个人中心
class PersonalPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Color(0xfff6f6f6) ,
      body: MyListView(
        enablePullDown: false,
        enablePullUp: false,
        children: <Widget>[
          HeaderWidget(),
          MyOrderWidget(),
          Divider(height: 10, thickness: 10, color: Color(0xfff6f6f6)),
          MyListTile(title: "购物车"),
          MyListTile(title: "优惠券"),
          Divider(height: 10, thickness: 10, color: Color(0xfff6f6f6)),
          MyListTile(title: "收藏夹"),
          MyListTile(title: "足迹"),
          Divider(height: 10, thickness: 10, color: Color(0xfff6f6f6)),
        ],
      ),
    );
  }
}

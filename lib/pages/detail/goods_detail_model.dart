import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:specialty_no1/modules/taobao.dart';

/// 商品详情数据处理
class GoodsDetailModel with ChangeNotifier {
  String itemId = "";
  dynamic seller = {};
  String title = "";
  String subtitle = "";
  List videos = [];
  dynamic consumerProtection = {};
  String priceText = "";
  String sellCount = "";
  List mainImages = [];
  List descImages = [];
  List groupProps = [];
  Future<void> setValue({String itemID}) async {
    await _request(itemID);
  }

  Future<void> _request(String itemID) async {
    try {
      var resImage = await Taobao.getDetailImage(itemID);
      var resInfo = await Taobao.getDetailInfo(itemID);
      var apiStackValue = json.decode(resInfo["data"]["apiStack"][0]["value"]);
      itemID = resInfo["data"]["item"]["itemId"];
      seller = resInfo["data"]["seller"];
      title = resInfo["data"]["item"]["title"];
      subtitle = resInfo["data"]["item"]["subtitle"];
      mainImages = resInfo["data"]["item"]["images"];
      groupProps = resInfo["data"]["props"]["groupProps"][0]["基本信息"];

      videos = apiStackValue["item"]["videos"];
      consumerProtection = apiStackValue["consumerProtection"];
      priceText = apiStackValue["price"]["price"]["priceText"];
      sellCount = apiStackValue["item"]["sellCount"];
      descImages = resImage;

      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}

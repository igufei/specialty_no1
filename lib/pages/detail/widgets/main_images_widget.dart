import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class MainImageWidget extends StatelessWidget {
  final List mainImages;

  const MainImageWidget({Key key, @required this.mainImages}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,
      //margin: EdgeInsets.only(bottom: 10),
      child: Builder(
        builder: (context) {
          if (mainImages.length > 0) {
            return Swiper(
              autoplayDelay: 10000,
              itemCount: mainImages.length,
              itemBuilder: (context, index) {
                String url = mainImages[index];
                url = url.startsWith("http:") ? url : "http:" + url;
                return Image.network(url, fit: BoxFit.fill);
              },
              pagination: SwiperPagination(builder: DotSwiperPaginationBuilder(color: Colors.black54, activeColor: Colors.white)),
              controller: SwiperController(),
              scrollDirection: Axis.horizontal,
              autoplay: false,
              onTap: (index) {},
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/detail/goods_detail_model.dart';
import 'package:specialty_no1/widgets/colors.dart';

/// 商品基础信息
class BasicInfoWidget extends StatelessWidget {
  final GoodsDetailModel model;

  const BasicInfoWidget({Key key, @required this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      //color: Colors.blue,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text.rich(TextSpan(style: TextStyle(color: MyColors.mainColor), children: [
                    TextSpan(text: "¥", style: TextStyle(fontSize: MySize.S(26))),
                    TextSpan(text: model.priceText + " ", style: TextStyle(fontSize: MySize.S(36))),
                    TextSpan(
                        text: "¥" + model.priceText, style: TextStyle(fontSize: MySize.S(24), color: MyColors.gray, decoration: TextDecoration.lineThrough)),
                  ])),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Container(
                      width: MySize.V(50),
                      alignment: Alignment.center,
                      child: Text("包邮", style: TextStyle(color: Colors.white, fontSize: MySize.S(16))),
                      decoration: BoxDecoration(color: MyColors.mainColor, borderRadius: BorderRadius.circular(5)),
                    ),
                  )
                ],
              ),
              Text(
                (model.sellCount ?? "0") + "人已购",
                style: TextStyle(
                  fontSize: MySize.S(24),
                  color: MyColors.gray,
                ),
              )
            ],
          ),
          Text(
            model.title,
            style: TextStyle(fontSize: MySize.S(26)),
          )
        ],
      ),
    );
  }
}

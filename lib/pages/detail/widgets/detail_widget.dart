import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/detail/goods_detail_model.dart';

class DetailWidget extends StatefulWidget {
  final GoodsDetailModel model;

  const DetailWidget({Key key, @required this.model}) : super(key: key);
  @override
  _DetailWidgetState createState() => _DetailWidgetState();
}

class _DetailWidgetState extends State<DetailWidget> {
  int currentIndex = 0;
  List tabs = ["图文详情", "商品参数"];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _tabBarTile(),
          Builder(
            builder: (context) {
              if (currentIndex == 0) {
                return _detailTile();
              } else {
                return _parameterTile();
              }
            },
          )
        ],
      ),
    );
  }

  Widget _tabBarTile() {
    Color basicColor = Color(0xff666666);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Container(
        height: MySize.V(50),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: basicColor),
        ),
        child: Row(
          children: tabs.asMap().keys.map((index) {
            TextStyle textStyle = TextStyle(fontSize: MySize.S(24), color: basicColor);
            Color bgColor = Colors.transparent;
            if (index == currentIndex) {
              textStyle = TextStyle(fontSize: MySize.S(24), color: Colors.white);
              bgColor = basicColor;
            }
            return Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    currentIndex = index;
                  });
                },
                child: Container(
                  color: bgColor,
                  alignment: Alignment.center,
                  child: Text(
                    tabs[index],
                    style: textStyle,
                  ),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }

  Widget _detailTile() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: widget.model.descImages.map((url) {
          url = url.startsWith("http:") ? url : "http:" + url;
          return Container(
            child: Image.network(url),
          );
        }).toList(),
      ),
    );
  }

  Widget _parameterTile() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: widget.model.groupProps.map((item) {
          return Row(
            children: <Widget>[
              Container(
                width: MySize.V(200),
                child: Text(
                  item.keys.first,
                  style: TextStyle(fontSize: MySize.S(22), color: Color(0xffbbbbbb)),
                ),
              ),
              Expanded(
                child: Text(
                  item.values.first,
                  style: TextStyle(
                    fontSize: MySize.S(22),
                    color: Color(0xff4a4a4a),
                  ),
                ),
              )
            ],
          );
        }).toList(),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/icons.dart';

class TopBarWidget extends StatefulWidget {
  @override
  _TopBarWidgetState createState() => _TopBarWidgetState();
}

class _TopBarWidgetState extends State<TopBarWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: MyColors.mainColor,
      padding: EdgeInsets.only(top: MySize.statusBarHeight,left: 10,right: 10,bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          CircleAvatar(
            backgroundColor: Colors.black45,
            child: BackButton(color: Colors.white,),
          ),
          CircleAvatar(
            backgroundColor: Colors.black45,
            child: Icon(MyIcons.shoppingCart1,size: MySize.S(30),color: Colors.white,),
          ),
        ],
      ),
    );
  }
}

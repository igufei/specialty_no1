import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/icons.dart';

class BottomBarWidget extends StatefulWidget {
  final String itemID;

  const BottomBarWidget({Key key, @required this.itemID}) : super(key: key);
  @override
  _BottomBarWidgetState createState() => _BottomBarWidgetState();
}

class _BottomBarWidgetState extends State<BottomBarWidget> {
  List iconBtns = [
    {
      "icon": MyIcons.chat,
      "title": "客服",
    },
    {
      "icon": MyIcons.favorite1,
      "title": "收藏",
    },
    {
      "icon": MyIcons.shoppingCart1,
      "title": "购物车",
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MySize.V(80),
      decoration: new BoxDecoration(
        color: Colors.white,
        boxShadow: [BoxShadow(color: MyColors.gray, offset: Offset(0, 2), blurRadius: 5.0)],
      ),
      child: Row(
        children: <Widget>[
          _iconsBtnsWidget(),
          _buyBtnsWidget(),
        ],
      ),
    );
  }

  Widget _iconsBtnsWidget() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Row(
        children: iconBtns.map((item) {
          return GestureDetector(
            child: Container(
              width: MySize.V(55),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 3),
                    child: Icon(
                      item["icon"],
                      color: Color(0xff999999),
                      size: MySize.S(28),
                    ),
                  ),
                  Text(
                    item["title"],
                    style: TextStyle(color: Color(0xff999999), fontSize: MySize.S(14)),
                  ),
                ],
              ),
            ),
            onTap: () {},
          );
        }).toList(),
      ),
    );
  }

  Widget _buyBtnsWidget() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 7),
        child: Row(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  decoration:
                      BoxDecoration(color: Color(0xffFFC500), borderRadius: BorderRadius.only(topLeft: Radius.circular(40), bottomLeft: Radius.circular(40))),
                  child: Text(
                    "加入购物车",
                    style: TextStyle(color: Colors.white, fontSize: MySize.S(22)),
                  ),
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  decoration:
                      BoxDecoration(color: Color(0xffFF7A00), borderRadius: BorderRadius.only(topRight: Radius.circular(40), bottomRight: Radius.circular(40))),
                  child: Text("立即购买",
                    style: TextStyle(color: Colors.white, fontSize: MySize.S(22)),),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

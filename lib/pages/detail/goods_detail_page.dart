import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/pages/detail/goods_detail_model.dart';
import 'package:specialty_no1/pages/detail/widgets/basic_info_widget.dart';
import 'package:specialty_no1/pages/detail/widgets/bottom_bar.dart';
import 'package:specialty_no1/pages/detail/widgets/detail_widget.dart';
import 'package:specialty_no1/pages/detail/widgets/main_images_widget.dart';
import 'package:specialty_no1/pages/detail/widgets/top_bar.dart';
import 'package:specialty_no1/widgets/my_list_view.dart';

/// 商品详情页面
class GoodsDetailPage extends StatelessWidget {
  final GoodsDetailModel _model = GoodsDetailModel();
  final String auctionId;
  GoodsDetailPage({this.auctionId}) {
    _model.setValue(itemID: this.auctionId);
  }
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: _model)],
      child: Scaffold(
        body: Container(
          child: Consumer<GoodsDetailModel>(
            builder: (context, value, widget) => Stack(
              children: <Widget>[
                MyListView(
                  enablePullDown: false,
                  enablePullUp: false,
                  children: <Widget>[
                    MainImageWidget(mainImages: _model.mainImages),
                    BasicInfoWidget(model: _model),
                    DetailWidget(model: _model),
                  ],
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: TopBarWidget(),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: BottomBarWidget(itemID: auctionId),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

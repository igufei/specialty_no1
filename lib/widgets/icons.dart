import 'package:flutter/material.dart';

/// 自定义icon
class MyIcons {
  static IconData get home => IconData(0xe61e, fontFamily: 'iconfont');
  static IconData get recommended => IconData(0xe609, fontFamily: 'iconfont');
  static IconData get classification => IconData(0xe612, fontFamily: 'iconfont');
  static IconData get shoppingCart => IconData(0xe606, fontFamily: 'iconfont');
  static IconData get my => IconData(0xe617, fontFamily: 'iconfont');
  static IconData get qrcode => IconData(0xe623, fontFamily: 'iconfont');
  static IconData get search => IconData(0xe622, fontFamily: 'iconfont');
  static IconData get list1 => IconData(0xe60c, fontFamily: 'iconfont');
  static IconData get list2 => IconData(0xe65e, fontFamily: 'iconfont');

  static IconData get favorite1 => IconData(0xe610, fontFamily: 'iconfont');
  static IconData get favorite2 => IconData(0xe630, fontFamily: 'iconfont');
  static IconData get shoppingCart1 => IconData(0xe656, fontFamily: 'iconfont');
  static IconData get chat => IconData(0xe646, fontFamily: 'iconfont');
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/colors.dart';

/// 商品列表排列类型
enum LayoutType {
  /// 网络
  Grid,

  /// 列表
  Column,
}

/// 默认商品项展示
class GoodsItem extends StatelessWidget {
  final LayoutType layoutType;
  final dynamic goodsData;
  GoodsItem({this.layoutType = LayoutType.Grid, @required this.goodsData});
  @override
  Widget build(BuildContext context) {
    if (layoutType == LayoutType.Grid) {
      return _gridLayout(context);
    } else if (layoutType == LayoutType.Column) {
      return _columnLayout(context);
    } else {
      return Container();
    }
  }

  void _tapItem(BuildContext context, auctionId) {
    Application.router.navigateTo(context, "/goods?auctionId=$auctionId", transition: TransitionType.cupertino);
  }

  // 每行两个
  Widget _gridLayout(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(0),
      elevation: 0.1,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: SizedBox(
        width: MySize.V(250),
        height: MySize.V(340),
        child: GestureDetector(
          onTap: () => _tapItem(context, this.goodsData["auctionId"]),
          child: Column(
            children: <Widget>[
              // 商品图片
              SizedBox(
                width: MySize.V(250),
                height: MySize.V(250),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10),
                  ),
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    placeholder: (ctx, url) => Padding(padding: EdgeInsets.all(MySize.V(60)), child: CircularProgressIndicator()),
                    imageUrl: this.goodsData["pictUrl"],
                    errorWidget: (ctx, url, err) => Image.asset('assets/images/error.png'),
                  ),
                ),
              ),

              // 标题
              Padding(
                padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(
                        left: 5,
                        right: 5,
                      ),
                      child: Text(
                        "自营",
                        style: TextStyle(color: Color(0xffffffff), fontSize: MySize.S(16)),
                      ),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: MyColors.mainColor),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 3),
                        child: Text(
                          this.goodsData["title"],
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: MySize.S(20),
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, left: 8, right: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "¥ " + this.goodsData["zkPrice"].toString(),
                      style: TextStyle(
                        color: MyColors.mainColor,
                        fontSize: MySize.S(22),
                      ),
                    ),
                    Text(
                      this.goodsData["biz30day"].toString() + "人购买",
                      style: TextStyle(
                        color: MyColors.gray,
                        fontSize: MySize.S(18),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // 列表布局
  Widget _columnLayout(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(0),
      elevation: 0.1,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: SizedBox(
        width: double.infinity,
        height: MySize.V(200),
        child: GestureDetector(
          onTap: () => _tapItem(context, this.goodsData["auctionId"]),
          child: Container(
            color: Colors.transparent,
            child: Row(
              children: <Widget>[
                // 商品图片
                SizedBox(
                  width: MySize.V(200),
                  height: MySize.V(200),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      placeholder: (ctx, url) => Padding(padding: EdgeInsets.all(MySize.V(60)), child: CircularProgressIndicator()),
                      imageUrl: this.goodsData["pictUrl"],
                      errorWidget: (ctx, url, err) => Image.asset('assets/images/error.png'),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        // 标题
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 3),
                                  child: Text(
                                    this.goodsData["title"],
                                    style: TextStyle(
                                      color: Color(0xff333333),
                                      fontSize: MySize.S(22),
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            alignment: Alignment.center,
                            width: MySize.V(50),
                            child: Text(
                              "自营",
                              style: TextStyle(color: Color(0xffffffff), fontSize: MySize.S(16)),
                            ),
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: MyColors.mainColor),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text.rich(TextSpan(
                                style: TextStyle(color: MyColors.mainColor),
                                children: [
                                  TextSpan(text: "¥ ", style: TextStyle(color: MyColors.mainColor, fontSize: MySize.S(24))),
                                  TextSpan(text: this.goodsData["zkPrice"].toString(), style: TextStyle(color: MyColors.mainColor, fontSize: MySize.S(28))),
                                ],
                              )),
                              Text(
                                this.goodsData["biz30day"].toString() + "人购买",
                                style: TextStyle(
                                  color: MyColors.gray,
                                  fontSize: MySize.S(20),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

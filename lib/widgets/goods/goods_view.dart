import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_model.dart';
import 'package:specialty_no1/widgets/goods/goods_item.dart';
import 'package:specialty_no1/widgets/goods/goods_item_time_buy.dart';
import 'package:specialty_no1/widgets/goods/goods_model.dart';
import 'package:specialty_no1/widgets/my_list_view.dart';
import 'dart:math' as math;

enum GoodsViewStyle { card, ranking, timeBuy }

/// 负责商品列表页面的展示
class GoodsView extends StatefulWidget {
  final GoodsModel model;
  final LayoutType layoutType;
  final ScrollController scrollController;
  final bool enablePullDown;
  final bool enablePullUp;
  final GoodsViewStyle viewStyle;
  final TimeState timeState;
  const GoodsView({
    Key key,
    @required this.model,
    this.layoutType = LayoutType.Grid,
    this.scrollController,
    this.enablePullDown = true,
    this.enablePullUp = true,
    this.viewStyle = GoodsViewStyle.card,
    this.timeState,
  }) : super(key: key);
  @override
  _GoodsViewState createState() => _GoodsViewState();
}

class _GoodsViewState extends State<GoodsView> {
  double spacing = MySize.V(10);
  @override
  void initState() {
    super.initState();
    if (widget.viewStyle == GoodsViewStyle.timeBuy) {
      spacing = 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyListView(
      controller: widget.scrollController,
      enablePullDown: widget.enablePullDown,
      enablePullUp: widget.enablePullUp,
      padding: EdgeInsets.symmetric(horizontal: spacing, vertical: spacing),
      header: MaterialClassicHeader(),
      children: [
        Wrap(
          spacing: spacing,
          runSpacing: spacing,
          alignment: WrapAlignment.center,
          children: widget.model.data.asMap().keys.map((index) {
            switch (widget.viewStyle) {
              case GoodsViewStyle.card:
                // 默认显示
                return _cartView(index);

              case GoodsViewStyle.ranking:
                // 显示名次
                return _rankingView(index);

              case GoodsViewStyle.timeBuy:

                // 显示抢购
                return _timeBuyView(index);

              default:
                return _cartView(index);
            }
          }).toList(),
        )
      ],
      onLoading: _onLoading,
      onRefresh: _onRefresh,
    );
  }

  /// 上拉刷新数据
  void _onRefresh(RefreshController rc) async {
    await widget.model.refresh();
    rc.refreshCompleted();
  }

  /// 下拉加载数据
  void _onLoading(RefreshController rc) async {
    await widget.model.setValue(sort: widget.model.pageNum + 1);
    rc.loadComplete();
  }

  Widget _cartView(int index) {
    return GoodsItem(
      goodsData: widget.model.data[index],
      layoutType: widget.layoutType,
    );
  }

  Widget _rankingView(int index) {
    String i = "";
    if (index <= 2) {
      i = (index + 1).toString();
    }
    return Stack(
      children: <Widget>[
        GoodsItem(
          goodsData: widget.model.data[index],
          layoutType: widget.layoutType,
        ),
        Container(
          width: MySize.V(50),
          height: MySize.V(50),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            image: DecorationImage(image: ExactAssetImage("assets/images/no$i.png"), alignment: Alignment.topLeft, fit: BoxFit.cover),
          ),
          child: Builder(
            builder: (context) {
              if (index < 3) {
                return Container();
              }
              return Padding(
                padding: EdgeInsets.only(right: MySize.V(13), bottom: MySize.V(13)),
                child: Transform.rotate(
                  angle: -math.pi / 4,
                  child: Text(
                    (index + 1).toString(),
                    style: TextStyle(color: Colors.white, fontSize: MySize.S(18)),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _timeBuyView(int index) {
    return GoodsItemTimeBuy(
      goodsData: widget.model.data[index],
      timeState: widget.timeState,
    );
  }
}

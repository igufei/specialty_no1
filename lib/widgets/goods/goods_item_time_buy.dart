import 'package:cached_network_image/cached_network_image.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_model.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:toast/toast.dart';

/// 限时抢购-商品项展示
class GoodsItemTimeBuy extends StatelessWidget {
  final TimeState timeState;
  final dynamic goodsData;
  GoodsItemTimeBuy({
    @required this.goodsData,
    @required this.timeState,
  });
  @override
  Widget build(BuildContext context) {
    return _columnLayout(context);
  }

  void _tapItem(BuildContext context, auctionId) {
    if (timeState == TimeState.futher) {
      Toast.show("活动未开始！", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundRadius: MySize.V(10));
    } else
      Application.router.navigateTo(context, "/goods?auctionId=$auctionId", transition: TransitionType.cupertino);
  }

  // 列表布局
  Widget _columnLayout(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      height: MySize.V(220),
      decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Color(0xffe7e7e7)))),
      child: GestureDetector(
        onTap: () => _tapItem(context, this.goodsData["auctionId"]),
        child: Container(
          color: Colors.transparent,
          child: Row(
            children: <Widget>[
              // 商品图片
              SizedBox(
                width: MySize.V(200),
                height: MySize.V(200),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  placeholder: (ctx, url) => Padding(padding: EdgeInsets.all(MySize.V(60)), child: CircularProgressIndicator()),
                  imageUrl: this.goodsData["pictUrl"],
                  errorWidget: (ctx, url, err) => Image.asset('assets/images/error.png'),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // 标题
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(left: 3),
                                child: Text(
                                  this.goodsData["title"],
                                  style: TextStyle(
                                    color: Color(0xff333333),
                                    fontSize: MySize.S(22),
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Baseline(
                            baseline: MySize.V(10),
                            baselineType: TextBaseline.alphabetic,
                            child: Text.rich(TextSpan(
                              style: TextStyle(color: MyColors.mainColor),
                              children: [
                                TextSpan(text: "¥ ", style: TextStyle(color: MyColors.mainColor, fontSize: MySize.S(24))),
                                TextSpan(text: this.goodsData["zkPrice"].toString(), style: TextStyle(color: MyColors.mainColor, fontSize: MySize.S(28))),
                              ],
                            )),
                          ),
                          Baseline(
                            baseline: MySize.V(10),
                            baselineType: TextBaseline.alphabetic,
                            child: Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                "¥ " + (this.goodsData["couponAmount"] + this.goodsData["zkPrice"]).toString(),
                                style: TextStyle(color: MyColors.gray, fontSize: MySize.S(20), decoration: TextDecoration.lineThrough),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Builder(
                              builder: (context) {
                                return timeState == TimeState.futher
                                    ? Container()
                                    : Container(
                                        width: MySize.V(150),
                                        height: MySize.V(20),
                                        decoration: BoxDecoration(
                                          color: Color(0xffffafaf),
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        child: Builder(
                                          builder: (context) {
                                            int totalCount = this.goodsData["couponTotalCount"] as int;
                                            int leftCount = this.goodsData["couponLeftCount"] as int;
                                            double v = (totalCount - leftCount) / totalCount;
                                            int grabRate = (v * 100).round() + 13 > 100 ? 99 : (v * 100).round() + 13;
                                            double grabValue = v * 150 + 20 > 150 ? 145 : v * 150 + 20;
                                            return Stack(
                                              children: <Widget>[
                                                Container(
                                                  width: MySize.V(grabValue),
                                                  height: MySize.V(20),
                                                  decoration: BoxDecoration(
                                                    color: MyColors.mainColor,
                                                    borderRadius: BorderRadius.circular(10),
                                                  ),
                                                  //color: MyColors.mainColor,
                                                ),
                                                Container(
                                                  alignment: Alignment.center,
                                                  child: Text("已抢$grabRate%", style: TextStyle(fontSize: MySize.S(14), color: Colors.white)),
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      );
                              },
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              alignment: Alignment.center,
                              width: MySize.V(120),
                              height: MySize.V(40),
                              decoration: BoxDecoration(
                                  color: timeState == TimeState.futher ? Color(0xff4ac201) : MyColors.mainColor, borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                timeState == TimeState.futher ? "待抢购" : "立即抢",
                                style: TextStyle(color: Colors.white, fontSize: MySize.S(20)),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

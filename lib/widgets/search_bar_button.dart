import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:specialty_no1/modules/size.dart';
import 'package:specialty_no1/routes/routes.dart';
import 'package:specialty_no1/widgets/colors.dart';
import 'package:specialty_no1/widgets/icons.dart';

/// 一个搜索形状的按钮
class SearchBarButton extends StatefulWidget {
  /// 不为空将启用输入框，不然只是一个button
  final TextEditingController controller;
  final void Function(String) onSubmitted;
  SearchBarButton({Key key, this.controller, this.onSubmitted}) : super(key: key);
  @override
  _SearchBarButtonState createState() => _SearchBarButtonState();
}

class _SearchBarButtonState extends State<SearchBarButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: MySize.V(12)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(MySize.V(20)),
        color: Colors.white,
      ),
      height: MySize.V(40),
      child: Builder(
        builder: (context) {
          if (widget.controller == null) {
            return GestureDetector(
              child: Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 5),
                      child: Icon(
                        MyIcons.search,
                        color: MyColors.gray,
                        size: MySize.S(28),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "输入关键词",
                        style: TextStyle(color: MyColors.gray, fontSize: MySize.S(20), fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                _tapSearchBar(context);
              },
            );
          }

          return CupertinoTextField(
            padding: EdgeInsets.only(top: 0),
            style: TextStyle(fontSize: MySize.S(24), textBaseline: TextBaseline.alphabetic),
            cursorWidth: 1.5,
            prefix: Padding(
              padding: EdgeInsets.only(left: 5),
              child: Icon(MyIcons.search, color: MyColors.gray, size: MySize.S(28)),
            ),
            placeholder: "输入关键词",
            placeholderStyle: TextStyle(fontSize: MySize.S(20), color: MyColors.gray),
            suffix: Padding(
              padding: EdgeInsets.only(right: 5),
              child: new InkWell(
                onTap: (() {
                  setState(() {
                    widget.controller.text = '';
                  });
                }),
                child: Icon(Icons.clear, color: MyColors.gray, size: MySize.S(28)),
              ),
            ),
            suffixMode: OverlayVisibilityMode.editing,
            autofocus: true,
            decoration: null,
            controller: widget.controller,
            onSubmitted: (text) => widget.onSubmitted(text),
            //enabled:false,
          );
        },
      ),
    );
  }

  // 点击搜索栏
  void _tapSearchBar(BuildContext context) {
    Application.router.navigateTo(context, "/search", transition: TransitionType.cupertino);
  }
}

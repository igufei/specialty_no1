import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:specialty_no1/modules/size.dart';
import 'effect.dart';

/// 自定义ListView 可以上拉刷新 下拉加载 去除 自带的水波效果
class MyListView extends StatefulWidget {
  final List<Widget> children;
  final Widget header;
  final Widget footer;
  final void Function(RefreshController refreshController) onRefresh;
  final void Function(RefreshController refreshController) onLoading;
  final ScrollController controller;
  final EdgeInsetsGeometry padding;
  final bool enablePullDown;
  final bool enablePullUp;
  MyListView({
    @required this.children,
    this.header,
    this.footer,
    this.onRefresh,
    this.onLoading,
    this.controller,
    this.padding,
    this.enablePullDown = true,
    this.enablePullUp = true,
  });
  @override
  _MyListViewState createState() => _MyListViewState();
}

class _MyListViewState extends State<MyListView> {
  final RefreshController _refreshController = RefreshController();
  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: OverScrollBehavior(),
      child: SmartRefresher(
        enablePullDown: widget.enablePullDown,
        enablePullUp: widget.enablePullUp,
        header: widget.header ??
            ClassicHeader(
              height: MySize.V(45),
              releaseText: '松开刷新',
              refreshingText: '刷新中',
              completeText: '刷新完成',
              failedText: '刷新失败',
              idleText: '下拉刷新',
              refreshingIcon: CupertinoActivityIndicator(),
            ),
        footer: widget.footer ??
            ClassicFooter(
              height: MySize.V(45),
              canLoadingText: '松开加载更多',
              failedText: '加载失败',
              idleText: '上拉加载',
              loadingText: '加载中',
              noDataText: '没有数据',
            ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView(
          children: widget.children,
          padding: widget.padding,
          controller: widget.controller,
          //physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
        ),
      ),
    );
  }

  void _onRefresh() {
    widget.onRefresh(_refreshController);
  }

  /// 下拉加载数据
  void _onLoading() {
    widget.onLoading(_refreshController);
  }
}

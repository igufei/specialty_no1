import 'package:flutter/material.dart';

/// 自定义颜色
class MyColors {
  static Color get mainColor => MaterialColor(
        0xffff464e,
        <int, Color>{
          50: Color(0xffff464e),
          100: Color(0xffff464e),
          200: Color(0xffff464e),
          300: Color(0xffff464e),
          400: Color(0xffff464e),
          500: Color(0xffff464e),
          600: Color(0xffff464e),
          700: Color(0xffff464e),
          800: Color(0xffff464e),
          900: Color(0xffff464e),
        },
      );

  static Color get gray => Color(0xff999999);
  static Color get bgColor => Color(0xfff6f7f8);
  static Color get textColor =>Color(0xff2b2929);
}

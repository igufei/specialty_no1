import 'package:flutter/widgets.dart';
import 'package:specialty_no1/widgets/goods/goods_item.dart';
/// 商品列表展示页面全局布局状态
class LayoutValue with ChangeNotifier {
  LayoutType _layoutType=LayoutType.Grid;

  LayoutType get layoutType => _layoutType;

  set layoutType(LayoutType _layoutType) {
    this._layoutType = _layoutType;
    notifyListeners();
  }
}

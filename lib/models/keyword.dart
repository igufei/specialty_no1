/// 频道商品关键词
class Keyword {
  ///  限时抢购
  static String get timeBuy => "特产 送礼";
  /// 有好货
  static String get greatGoods => "特产 台湾";
  /// 推荐
  static String get recommended => "特产 盐城";
  /// 排行榜
  static String get ranking => "特产";
  /// 猜你喜欢
  static String get guessULike => "特产 送礼 商务";
}

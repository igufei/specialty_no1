import 'package:flutter/widgets.dart';
/// 首页顶部状态栏透明值全局状态值 
class TopStatusBarOpacityValue with ChangeNotifier {
  double value;
  bool visible;
  TopStatusBarOpacityValue({this.value = 0.0, this.visible = true});
  setValue(double value) {
    this.value = value;
    notifyListeners();
  }

  setVisible(bool visible) {
    this.visible = visible;
    notifyListeners();
  }
}

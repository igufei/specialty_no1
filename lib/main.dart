import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:specialty_no1/app.dart';
import 'package:specialty_no1/models/layout_value.dart';
import 'package:specialty_no1/models/opacity_value.dart';
/// 程序主入口
void main() {
  if (Platform.isAndroid) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.dark,
    ));
  }

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider.value(value: TopStatusBarOpacityValue()),
      ChangeNotifierProvider.value(value: LayoutValue()),
    ],
    child: MyApp(),
  ));
}

import 'package:fluro/fluro.dart';
import 'package:specialty_no1/modules/utils.dart';
import 'package:specialty_no1/pages/404/404_page.dart';
import 'package:specialty_no1/pages/category/category_page.dart';
import 'package:specialty_no1/pages/detail/goods_detail_page.dart';
import 'package:specialty_no1/pages/goodslist/goods_list_page.dart';
import 'package:specialty_no1/pages/index/index_page.dart';
import 'package:specialty_no1/pages/notice/notice_page.dart';
import 'package:specialty_no1/pages/personal/personal_page.dart';
import 'package:specialty_no1/pages/recommended/recommended_page.dart';
import 'package:specialty_no1/pages/search/search_page.dart';
import 'package:specialty_no1/pages/shoppingcart/shopping_cart_page.dart';
import 'package:specialty_no1/pages/timebuy/time_buy_page.dart';

/// 路由表
class Routes {
  static void setup(FluroRouter router) {
    // 首页
    router.define(
      "/index",
      handler: Handler(handlerFunc: (context, params) => IndexPage()),
    );

    // 推荐
    router.define(
      "/recommended",
      handler: Handler(
          handlerFunc: (context, params) =>
              RecommendedPage(tabIndex: params["tabIndex"]?.first)),
    );

    // 分类
    router.define(
      "/category",
      handler: Handler(handlerFunc: (context, params) => CategoryPage()),
    );

    // 购物车
    router.define(
      "/shoppingcart",
      handler: Handler(handlerFunc: (context, params) => ShoppingCartPage()),
    );

    // 个人中心
    router.define(
      "/personal",
      handler: Handler(handlerFunc: (context, params) => PersonalPage()),
    );

    // 搜索
    router.define(
      "/search",
      handler: Handler(handlerFunc: (context, params) => SearchPage()),
    );

    // 商品详情
    router.define(
      "/goods",
      handler: Handler(
          handlerFunc: (context, params) =>
              GoodsDetailPage(auctionId: params["auctionId"]?.first)),
    );

    // 商品列表
    router.define(
      "/list",
      handler: Handler(
        handlerFunc: (context, params) {
          return GoodsListPage(
            title: MyText.decode(params["title"]?.first),
            keyword: MyText.decode(params["keyword"]?.first),
          );
        },
      ),
    );
    // 限时抢购
    router.define(
      "/timebuy",
      handler: Handler(handlerFunc: (context, params) => TimeBuyPage()),
    );
    // 通知
    router.define(
      "/notice",
      handler: Handler(
          handlerFunc: (context, params) =>
              NoticePage(id: params["id"]?.first)),
    );

    // 404
    router.notFoundHandler =
        Handler(handlerFunc: (context, params) => ErrorPage());
    Application.router = router;
  }
}

class Application {
  static FluroRouter router;
}

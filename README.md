### specialty_no1（特产1号）介绍

使用`flutter`开发的一款特产类商城`APP`，目前只测试过`android`系统，`ios`系统没有测试

页面所有数据都来自于个人服务器

<img src="./image/1.jpg" style="zoom: 33%;" />

<img src="./image/2.jpg" style="zoom: 33%;" />

<img src="./image/3.jpg" style="zoom: 33%;" />

<img src="./image/4.jpg" style="zoom: 33%;" />

<img src="./image/5.jpg" style="zoom: 33%;" />

